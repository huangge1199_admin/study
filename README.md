# study

### 介绍
个人学习项目汇总

### 包含项目

- tomcat-io：手写tomcat
  - bio：基于传统IO手写tomcat
  - nio：基于netty手写tomcat
- rpc-netty: netty实现RPC
- springcloud-nacos-seata：seata1.4.1应用demo
  
  部署文档：
  - https://blog.csdn.net/huangge1199/article/details/118151286?spm=1001.2014.3001.5501
  - https://blog.huangge1199.cn/post/seata141demo/
- vue-data-screen:vue可视化平台

![screen.png](./vue-data-screen/screen.png)
  