import Vue from 'vue'
import Router from 'vue-router'

Vue.use(Router)

// 路由
export const routes = [{
  path: '/',
  name: 'home',
  component: () => import('@/views/home/index')
},
{
  path: '/test',
  name: 'test',
  component: () => import('@/views/test/index')
}]

// 解决ElementUI导航栏中的vue-router在3.0版本以上重复点菜单报错问题
const originalPush = Router.prototype.push
Router.prototype.push = function push (location) {
  return originalPush.call(this, location).catch(err => err)
}

// 创建路由
const createRouter = () => new Router({
  routes: routes
})

const router = createRouter()

// 重置路由
export function resetRouter () {
  const newRouter = createRouter()
  router.matcher = newRouter.matcher
}

export default router
