package com.study.rpc.api;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 19:24
 */
public interface IRpcHelloService {
    String hello(String name);
}
