package com.study.rpc.api;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 21:19
 */
public interface IRpcService {

    int add(int a,int b);

    int sub(int a,int b);

    int mult(int a,int b);

    int div(int a,int b);
}
