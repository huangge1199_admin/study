package com.study.rpc.consumer;

import com.study.rpc.api.IRpcHelloService;
import com.study.rpc.api.IRpcService;
import com.study.rpc.consumer.proxy.RpcProxy;
import com.study.rpc.provider.RpcServiceImpl;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 19:25
 */
public class RpcConsumer {

    public static void main(String[] args) {

        IRpcHelloService rpcHello = RpcProxy.create(IRpcHelloService.class);
        System.out.println(rpcHello.hello("Tom"));

        IRpcService rpc = RpcProxy.create(IRpcService.class);
        System.out.println("8 + 2 = " + rpc.add(8, 2));
        System.out.println("8 - 2 = " + rpc.sub(8, 2));
        System.out.println("8 * 2 = " + rpc.mult(8, 2));
        System.out.println("8 / 2 = " + rpc.div(8, 2));

    }
}
