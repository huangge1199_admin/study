package com.study.rpc.protocol;

import lombok.Data;

import java.io.Serializable;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 19:20
 */
@Data
public class InvokerProtocol implements Serializable {

    private String className;

    private String methodName;

    private Class<?>[] params;

    private Object[] values;
}
