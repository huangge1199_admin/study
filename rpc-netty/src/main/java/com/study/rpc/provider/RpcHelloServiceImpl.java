package com.study.rpc.provider;

import com.study.rpc.api.IRpcHelloService;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 19:24
 */
public class RpcHelloServiceImpl implements IRpcHelloService {
    @Override
    public String hello(String name) {
        return "hello " + name + "!";
    }
}
