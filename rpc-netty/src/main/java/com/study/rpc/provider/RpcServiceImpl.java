package com.study.rpc.provider;

import com.study.rpc.api.IRpcService;

/**
 * @Author: huang
 * @Description TODO
 * @Date: 2021-01-16 21:20
 */
public class RpcServiceImpl implements IRpcService {
    @Override
    public int add(int a, int b) {
        return a + b;
    }

    @Override
    public int sub(int a, int b) {
        return a - b;
    }

    @Override
    public int mult(int a, int b) {
        return a * b;
    }

    @Override
    public int div(int a, int b) {
        return a / b;
    }
}
