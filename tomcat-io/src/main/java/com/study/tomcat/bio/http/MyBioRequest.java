package com.study.tomcat.bio.http;

import java.io.InputStream;

/**
 * @Author: huang
 * @Description 传统IO的请求
 * @Date: 2021-01-16 12:44
 */
public class MyBioRequest {

    private String url;

    private String method;

    public MyBioRequest(InputStream is) {
        try {
            String context = "";
            byte[] buff = new byte[2048];
            int len;
            if ((len = is.read(buff)) > 0) {
                context = new String(buff, 0, len);
            }
            String line = context.split("\\n")[0];
            String[] arr = line.split("\\s");

            this.method = arr[0];
            this.url = arr[1].split("\\?")[0];
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getUrl() {
        return this.url;
    }

    public String getMethod() {
        return this.method;
    }
}
