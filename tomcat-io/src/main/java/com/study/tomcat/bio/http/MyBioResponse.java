package com.study.tomcat.bio.http;

import java.io.OutputStream;
import java.nio.charset.StandardCharsets;

/**
 * @Author: huang
 * @Description 传统IO的响应
 * @Date: 2021-01-16 12:45
 */
public class MyBioResponse {

    private final OutputStream os;

    public MyBioResponse(OutputStream os) {
        this.os = os;
    }

    public void write(String s) throws Exception{
        String sb = "HTTP/1.1 200 OK\n" +
                "Content-Type: text/html;\n" +
                "\r\n" +
                s;
        os.write(sb.getBytes(StandardCharsets.UTF_8));
    }
}
