package com.study.tomcat.bio.http;

/**
 * @Author: huang
 * @Description 传统IO的服务
 * @Date: 2021-01-16 12:45
 */
public abstract class MyBioServlet {

    public void service(MyBioRequest request,MyBioResponse response) throws Exception{
        String get = "get";
        if(get.equalsIgnoreCase(request.getMethod())){
            doGet(request,response);
        }else{
            doPost(request,response);
        }
    }

    /**
     * get方法
     * @param request 请求
     * @param response 响应
     * @throws Exception 异常
     */
    public abstract void doGet(MyBioRequest request,MyBioResponse response) throws Exception;

    /**
     * post 方法
     * @param request 请求
     * @param response 响应
     * @throws Exception 异常
     */
    public abstract void doPost(MyBioRequest request,MyBioResponse response) throws Exception;
}
