package com.study.tomcat.bio.servlet;

import com.study.tomcat.bio.http.MyBioRequest;
import com.study.tomcat.bio.http.MyBioResponse;
import com.study.tomcat.bio.http.MyBioServlet;

/**
 * @Author: huang
 * @Description 参考例子：第一个请求
 * @Date: 2021-01-16 12:57
 */
public class MyFirstServlet extends MyBioServlet {
    @Override
    public void doGet(MyBioRequest request, MyBioResponse response) throws Exception {
        doPost(request,response);
    }

    @Override
    public void doPost(MyBioRequest request, MyBioResponse response) throws Exception {
        response.write("This is first servlet from bio....");
    }
}
