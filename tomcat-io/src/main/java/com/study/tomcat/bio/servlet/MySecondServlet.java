package com.study.tomcat.bio.servlet;

import com.study.tomcat.bio.http.MyBioRequest;
import com.study.tomcat.bio.http.MyBioResponse;
import com.study.tomcat.bio.http.MyBioServlet;

/**
 * @Author: huang
 * @Description 参考例子：第二个请求
 * @Date: 2021-01-16 12:58
 */
public class MySecondServlet extends MyBioServlet {
    @Override
    public void doGet(MyBioRequest request, MyBioResponse response) throws Exception {
        doPost(request,response);
    }

    @Override
    public void doPost(MyBioRequest request, MyBioResponse response) throws Exception {
        response.write("This is second servlet from bio....");
    }
}
