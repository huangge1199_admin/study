package com.study.tomcat.nio.http;

/**
 * @Author: huang
 * @Description 基于netty的服务
 * @Date: 2021-01-16 12:45
 */
public abstract class MyNioServlet {

    public void service(MyNioRequest request, MyNioResponse response) throws Exception{
        if("get".equalsIgnoreCase(request.getMethod())){
            doGet(request,response);
        }else{
            doPost(request,response);
        }
    }

    public abstract void doGet(MyNioRequest request, MyNioResponse response) throws Exception;

    public abstract void doPost(MyNioRequest request, MyNioResponse response) throws Exception;
}
