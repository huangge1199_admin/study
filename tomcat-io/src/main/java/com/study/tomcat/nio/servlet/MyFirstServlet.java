package com.study.tomcat.nio.servlet;

import com.study.tomcat.nio.http.MyNioRequest;
import com.study.tomcat.nio.http.MyNioResponse;
import com.study.tomcat.nio.http.MyNioServlet;

/**
 * @Author: huang
 * @Description 参考例子：第一个请求
 * @Date: 2021-01-16 12:57
 */
public class MyFirstServlet extends MyNioServlet {
    @Override
    public void doGet(MyNioRequest request, MyNioResponse response) throws Exception {
        doPost(request,response);
    }

    @Override
    public void doPost(MyNioRequest request, MyNioResponse response) throws Exception {
        response.write("This is first servlet from nio....");
    }
}
